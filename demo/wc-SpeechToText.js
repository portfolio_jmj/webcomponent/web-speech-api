const Template = document.createElement('template');
Template.innerHTML = `
<style>
    p{
        width: 100%;
        text-align: center;
    }
    main,section{
        display: flex;
        flex-wrap: wrap;
        align-items: center;
        align-content: center;
        justify-content: center;
    }
    button{
        aspect-ratio: 3/2;
        border-radius: 0.5rem;
        background-color: black;
        color: aliceblue;
    }
    button:hover{
        background-color: aliceblue;
        color: black;
    }
    .record{
        border: 2px solid rgb(99, 254, 99);
    }
    .stop{
        border: 2px solid rgb(250, 79, 79);
    }
    .display-none{
        display: none;
    }
</style>
<main>
    <p id="text"></p>
    <section>
        <button class="record">record</button>
        <button class="stop display-none">stop</button>
    </section>
</main>

`
class WcSpeechToText extends HTMLElement {
    html
    recognition
    btnRecord
    text

    constructor() {
        super();
        this.attachShadow({ mode: 'open'});
        this.shadowRoot.appendChild(Template.content.cloneNode(true));
        this.html = document.querySelector('html')
        this.text = this.shadowRoot.querySelector('#text');
        this.btnRecord = this.shadowRoot.querySelector('.record');
        this.btnStop = this.shadowRoot.querySelector('.stop');    

        let SpeechRecognition = undefined;
        try {
            if(window.SpeechRecognition)
                SpeechRecognition = window.SpeechRecognition;
            else if(webkitSpeechRecognition != undefined || webkitSpeechRecognition != null)
                SpeechRecognition = webkitSpeechRecognition;
        } catch (error) {
            console.info("Malheureusement vôtre navigateur n'implémente pas encore l'API Web Speech")
        }
        if(!SpeechRecognition){
            this.text.textContent = "Malheureusement vôtre navigateur n'implémente pas encore l'API Web Speech"
            this.btnRecord.classList.add('display-none')
            return
        }
        //const SpeechGrammarList = window.SpeechGrammarList || webkitSpeechGrammarList;
        //const SpeechRecognitionEvent = window.SpeechRecognitionEvent || webkitSpeechRecognitionEvent;
        //console.info('SpeechGrammarList',SpeechGrammarList)
        this.recognition = new SpeechRecognition();
        this.recognition.lang = this.html.lang
        this.recognition.continuous = false;
        this.recognition.interimResults = false;
        this.recognition.maxAlternatives = 1;
        this.text.textContent = "appuie sur le bouton record puis parle"
        
   
        this.btnRecord.addEventListener('click', () => this.record())
        this.btnStop.addEventListener('click',()=> this.stop())
    }

    connectedCallback() {
        if(!this.recognition)
            return
        this.recognition.onresult = (event) => { this.result(event) };

        this.recognition.onspeechend = () => {
            this.recognition.stop();
          };
          this.recognition.onerror = (event) => {
            console.error("onerror",event);
          };
    }

    disconnectedCallback() {
        //implementation
    }

    attributeChangedCallback(name, oldVal, newVal) {
        //implementation
    }

    adoptedCallback() {
        //implementation
    }

    record(){
        this.recognition.start();
        this.text.textContent = "je t'écoute... quand tu as finis appuie sur le bouton stop"
        this.btnRecord.classList.toggle('display-none');
        this.btnStop.classList.toggle('display-none');
    }

    stop(){
        this.recognition.stop();
    }

    result(pEvent){
        console.log(pEvent)
        const confidence = pEvent.results[0][0].confidence
        this.text.textContent = `${pEvent.results[0][0].transcript} (${(parseFloat(confidence).toPrecision(4)) * 100}%)`
        this.btnStop.classList.toggle('display-none');
        this.btnRecord.classList.toggle('display-none');
    }

}

window.customElements.define('wc-speechtotext', WcSpeechToText);